package Lesson1;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Human.serialize();
        System.out.println(Human.deserialize().toString());
    }
}
