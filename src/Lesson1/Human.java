package Lesson1;

import java.io.*;

public class Human implements Serializable {
    private int age;
    private String name;

    public Human(int age, String name){
        this.age = age;
        this.name = name;
    }

    public static Human deserialize() throws IOException, ClassNotFoundException {
        Human human = null;
        FileInputStream fiStream = new FileInputStream("human.ser");
        ObjectInputStream oiStream = new ObjectInputStream(fiStream);
        human = (Human) oiStream.readObject();
        return human;
    }

    public static void serialize() throws IOException {
        Human human = new Human(12, "Ivan");
        FileOutputStream foStream = new FileOutputStream("human.ser");
        ObjectOutputStream ooStream = new ObjectOutputStream(foStream);
        ooStream.writeObject(human);
    }

    @Override
    public String toString() {
        return "Age: " + age + ", Name: " + name;
    }
}

