package Lesson1;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Task1 {
    public static void read(String fileLocation) throws IOException {
        List<String> lines = Files.lines(Paths.get(fileLocation), StandardCharsets.UTF_8)
                .collect(Collectors.toList());
        for (String string : lines
        ) {
            System.out.println(string);
        }
    }

    public static void main(String[] args) throws IOException {
        read("C:\\Users\\juliaa_sha\\Desktop\\ELEKS_QA_Camp.txt");
    }
}
