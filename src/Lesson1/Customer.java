package Lesson1;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.File;


@XmlRootElement(name = "customer")
@XmlType(propOrder = {"id", "age", "name"})
public class Customer {
    private int id;
    private int age;
    private String name;

    public Customer(){

    }

    public Customer(int id, int age, String name) {
        this.id = id;
        this.age = age;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void writeCustomer() throws JAXBException {
        Customer customer = new Customer(1, 40, "Valdemar");
        File file = new File("Customer.xml");
        JAXBContext context = JAXBContext.newInstance(Customer.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.marshal(customer, file);
    }

    public static void readCustomer() throws JAXBException {
        File file = new File("Customer.xml");
        JAXBContext context = JAXBContext.newInstance(Customer.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        Customer customer = (Customer) unmarshaller.unmarshal(file);
        System.out.println(customer.toString());
    }

    public static void main(String[] args) throws JAXBException {
        writeCustomer();
        readCustomer();
    }

    @Override
    public String toString(){
        return "Id: " + id + ", age: " + age + ", name: " + name;
    }
}


