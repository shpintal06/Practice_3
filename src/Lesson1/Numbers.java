package Lesson1;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Numbers {

    public static void main(String[] args) {
        double[] numbers = new double[] {1, 0.2, 3, 6, 5,6.1};

        List<Double> list = Arrays.stream(numbers)
                .boxed().collect(Collectors.toList());
        System.out.println("Min");
        System.out.println(list.stream().mapToDouble(number -> number).min());
        System.out.println("Max");
        System.out.println(list.stream().mapToDouble(number -> number).max());
        System.out.println("Average");
        System.out.println(list.stream().mapToDouble(number -> number).average());

        System.out.println("Sorting");

        list.stream()
                .sorted()
                .forEach(System.out::println);

        System.out.println("Filtering");

        list.stream()
                .filter(number -> number > 3)
                .forEach(System.out::println);
    }
}
