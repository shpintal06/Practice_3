package Lesson1;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesCreator {
    public static void main(String[] args) throws IOException {
       createProperties();
       writeProperties();
       readProperties();
    }

    public static void createProperties() throws IOException {
        Properties properties = new Properties();
        FileOutputStream fileOutputStream = new FileOutputStream("config.properties");
        properties.store(fileOutputStream, "Properties Test");
    }

    public static void writeProperties() throws IOException {
        Properties properties = new Properties();
        properties.setProperty("browser_name", "Chrome");
        properties.setProperty("browser_version", "70");
        FileOutputStream fileOutputStream = new FileOutputStream("config.properties");
        properties.store(fileOutputStream, "Properties Test");

    }
    public static void readProperties() throws IOException {
        Properties properties = new Properties();
        FileInputStream fileInputStream = new FileInputStream("config.properties");
        properties.load(fileInputStream);
        System.out.println(properties.getProperty("browser_name"));
    }
}
