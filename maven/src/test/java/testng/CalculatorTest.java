package testng;

import Main.Calculator;
import org.testng.Assert;
import org.testng.annotations.*;

public class CalculatorTest {

    private double hardcodeValue = 0;

    @BeforeTest
    public void setHardcodeValue(){
        hardcodeValue = 5;
    }

    @AfterTest
    public void printStatus(){
        System.out.println("Test is completed");
    }


    @Test (groups = "correct")
    public void checkIfSumOfTwoValuesIsCorrect() {
        double b = 6;
        double expected = 11;

        Assert.assertEquals(expected, Calculator.add(hardcodeValue, b));
    }

    @Parameters({"a", "b"})
    @Test (groups = "unCorrect")
    public void checkIfSumOfTwoValueIsUnCorrect(double a, double b) {
        double expected = 10;

        Assert.assertEquals(expected, Calculator.add(a, b), "Wrong expected result");
    }

    @Test (groups = "correct")
    public void checkIfSubtractionOfTwoValuesIsCorrect() {
        double a = 2;
        double b = 3;
        double expected = -1;

        Assert.assertEquals(expected, Calculator.subtract(a, b));
    }

    @Test (groups = "unCorrect")
    public void checkIfSubtractionOfTwoValuesIsUnCorrect() {
        double a = 2;
        double b = 3;
        double expected = 5;

        Assert.assertEquals(expected, Calculator.subtract(a, b));
    }

    @Test (groups = "unCorrect")
    public void checkIfDividingOfTwoValuesIsUnCorrect() {
        double a = 10;
        double b = 2;
        double expected = 1;

        Assert.assertNotEquals(expected, Calculator.divide(a, b));
    }

    @Test (groups = "unCorrect")
    public void checkIfMultiplicationOfTwoValuesIsUnCorrect() {
        double a = 7;
        double b = 8;
        double expected = 56;

        Assert.assertNotEquals(expected, Calculator.multiply(a, b));
    }

    @DataProvider(name = "Sum")
    public  static Object[][] getValues() {
        return new Object[][]{{5d, 6d}, {4d, 7d}};
    }

    @Test(dataProvider = "Sum")
    public void testingSumWithDataDrivenTesting(double a, double b) {
        double expected = 11;
        Assert.assertEquals(expected, Calculator.add(a, b));
    }
}
